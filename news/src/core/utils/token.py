from datetime import datetime, timedelta

from fastapi import HTTPException
from jose import jws,jwt


SECRET_KEY="0c9d5fee5b70fc14ab3f02f07853bfea0316e73a70b26fa3a0808e5280aac766"
ACCESS_TOKEN_EXPIRE_MINUTES=30
ALGORITHM = "HS256"



class Token:
    @staticmethod
    def get_token(user_dict:dict):
        access_time = timedelta(ACCESS_TOKEN_EXPIRE_MINUTES)
        expire = datetime.utcnow() + access_time
        user_dict.update({"exp":expire})
        print(user_dict)
        print(type(user_dict))
        encoded_jwt=jwt.encode(user_dict,SECRET_KEY,algorithm=ALGORITHM)
        return encoded_jwt

    @staticmethod
    def get_dict(token:str):
        token=token[7:]
        try:
            decoded_token=jwt.decode(token,SECRET_KEY)
        except Exception as e:
            print(e)
            raise HTTPException(status_code=401,detail="JWT is not valid")
        # else:
        #     decoded_token = jwt.decode(token, SECRET_KEY)
        email=decoded_token["email"]
        return email

    @staticmethod
    def get_details(token: str):
        token = token[7:]
        try:
            decoded_token = jwt.decode(token, SECRET_KEY)
        except Exception as e:
            print(e)
            raise HTTPException(status_code=401, detail="JWT is not valid")
        # else:
        #     decoded_token = jwt.decode(token, SECRET_KEY)

        return decoded_token