import uuid

from sqlalchemy import Column,String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from app.v1.models import NewsModel

from core import Base

class TagModel(Base):
    __tablename__ = "Tags"

    tag_id = Column(UUID(as_uuid=True),primary_key=True)
    tag_name=Column(String(70))
    news=relationship("NewsModel",secondary="tags_news",lazy="selectin",back_populates="tags")


    @classmethod
    def create(cls,tag_name:str):
        return cls(tag_id=uuid.uuid4(),tag_name=tag_name)
