import uuid
from datetime import date

from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, DateTime
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from core import Base


class NewsModel(Base):
    __tablename__ = "News"

    news_id = Column(UUID(as_uuid=True), primary_key=True)
    news_title = Column(String(70))
    news_content = Column(String(70))
    is_active = Column(Boolean,default=False)
    publish_date = Column(DateTime)
    city_id = Column(UUID(as_uuid=True),ForeignKey("City.city_id"))

    user_id = Column(UUID(as_uuid=True),ForeignKey("Users.user_id"))
    likes = relationship("LikeModel",lazy="selectin")
    comments = relationship("CommentModel",lazy="selectin")
    category = relationship("CategoryModel",secondary="category_news",lazy="selectin",back_populates="news")
    tags = relationship("TagModel",secondary="tags_news",lazy="selectin",back_populates="news")
    like_count=Column(Integer)
    comment_count=Column(Integer)

    @classmethod
    def create(cls,news_title:str,news_content:str,publish_date:date,city_id:UUID,user_id:UUID):
        return cls(news_id=uuid.uuid4(), news_title=news_title, news_content=news_content, is_active=False,
                         publish_date=publish_date, city_id=city_id, user_id=user_id, likes=[], comments=[],
                         category=[], tags=[], like_count=0, comment_count=0)
