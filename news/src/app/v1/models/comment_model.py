import uuid
from datetime import date

from sqlalchemy import Column,Integer,String, ForeignKey, Date
from sqlalchemy.dialects.postgresql import UUID

from core import Base

class CommentModel(Base):
    __tablename__ = "Comments"

    comment_id=Column(UUID(as_uuid=True),primary_key=True)
    comment_content=Column(String(100))
    user_id = Column(UUID(as_uuid=True), ForeignKey("Users.user_id"))
    news_id = Column(UUID(as_uuid=True), ForeignKey("News.news_id"))
    comment_date=Column(Date)


    @classmethod
    def create(cls,comment_content:str,user_id:UUID,news_id:UUID,comment_date:date):
        return cls(comment_id=uuid.uuid4(),comment_content=comment_content,user_id=user_id,news_id=news_id,comment_date=comment_date)