from sqlalchemy import Column, ForeignKey, Integer, DateTime, String
from sqlalchemy.dialects.postgresql import UUID
import uuid
from datetime import datetime, timedelta
import random
from core import Base


class OtpModel(Base):
    __tablename__="Otp"
    otp_id = Column(UUID(as_uuid=True),primary_key=True)
    email=Column(String,ForeignKey("Users.email"))
    otp=Column(Integer)
    exp_date=Column(DateTime)


    @classmethod
    def create(cls,email:str):
        otp_id=uuid.uuid4()
        otp=random.randint(1000,9999)
        time_limit=timedelta(minutes=15)
        exp_date=datetime.utcnow()+time_limit
        return cls(otp_id=otp_id,email=email,otp=otp,exp_date=exp_date)
