from core import Base
from sqlalchemy import Table,ForeignKey, Column

tags_news=Table("tags_news",Base.metadata,
                Column("news_id",ForeignKey("News.news_id")),
                Column("tag_id",ForeignKey("Tags.tag_id"))
                )