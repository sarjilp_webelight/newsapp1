from uuid import UUID

from app.v1.repository import TagsNewsRepo
from fastapi import Depends



class TagsNewsService:
    def __init__(self,tags_news_repo:TagsNewsRepo=Depends(TagsNewsRepo)):
        self.tags_news_repo=tags_news_repo

    async def addNewsTags(self,news_id:UUID,tag_id:UUID):
        #print(news_id,category_id)
        news=await self.tags_news_repo.getNews(news_id=news_id)
        print(news)
        tag=await self.tags_news_repo.getTag(tag_id=tag_id)
        news.tags.append(tag)
        print("2 is here")
        return await self.tags_news_repo.addTagNews(news)