from uuid import UUID

from app.v1.repository import CategoryNewsRepo
from fastapi import Depends



class CategoryNewsService:
    def __init__(self,category_news_repo:CategoryNewsRepo=Depends(CategoryNewsRepo)):
        self.category_news_repo=category_news_repo

    async def addNewsCategory(self,news_id:UUID,category_id:UUID):
        print(news_id,category_id)
        news=await self.category_news_repo.getNews(news_id=news_id)
        print(news)
        category=await self.category_news_repo.getCategory(category_id=category_id)
        news.category.append(category)
        print("2 is here")
        return await self.category_news_repo.addCategoryNews(news)