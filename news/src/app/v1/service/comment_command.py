from datetime import date

from fastapi import Depends, HTTPException
from sqlalchemy import Date

from app.v1.repository import CommentRepo
from app.v1.models import CommentModel,NewsModel
import uuid
from uuid import UUID
from app.v1.schema import CreateCommentRequestSchema,CreateCommentResponse
class CommentCommandService:
    def __init__(
            self,
            comment_repo:CommentRepo=Depends(CommentRepo)
    ):
        self.comment_repo=comment_repo


    async def create_comment(
            self,
            comment_content:str,
            user_id:UUID,
            news_id:UUID,
            comment_date:date
    ):
        comment=CommentModel.create(comment_content,user_id,news_id,comment_date)
        comment=await self.comment_repo.save(comment=comment)
        await self.comment_repo.session.commit()
        news =await self.comment_repo.getCommentCount(news_id)
        print(news.comment_count)
        news.comment_count=news.comment_count+1
        await self.comment_repo.setCommentCount(news)
        return comment

    async def read_comment(self):
        result= await self.comment_repo.get_comment()
        return result


    async def read_commentid(self,comment_id:UUID):
        result = await self.comment_repo.get_commentid(comment_id)
        return result



    async def delete_comment(self,comment_id:UUID,user_id:UUID):
        # user= self.user_repo.get_id(user_id=user_id)
        # print(user)
        comment=await self.comment_repo.get_commentid(comment_id)
        print("pass1",user_id)
        print("pass:",comment.user_id)
        print(comment.user_id==user_id)
        if str(comment.user_id)!=user_id:
            raise HTTPException(status_code=401,detail="Only User who commented can delete this comment")
        news_id=comment.news_id
        news = await self.comment_repo.getCommentCount(news_id)
        print(news.comment_count)
        news.comment_count = news.comment_count - 1
        await self.comment_repo.setCommentCount(news)
        return await self.comment_repo.delete_comment(comment_id)

    async def update_comment(self,comment_id:UUID,request:CreateCommentRequestSchema):
        return await self.comment_repo.update_comment(comment_id,request)