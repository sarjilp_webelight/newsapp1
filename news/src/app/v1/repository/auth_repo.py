from fastapi import Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select
from core.utils.hashing import Hasher
from app.v1.models import UserModel,OtpModel
from app.v1.schema import CreateUserResponse
from core import db_session
from uuid import UUID



class AuthRepo:

    def __init__(self,db:AsyncSession=Depends(db_session)):
        self.session=db


    async def save(self,user:UserModel):
        self.session.add(user)
        await self.session.commit()
        return user


    async def check_email(self,email:str):
        check = await self.session.execute(select(UserModel).where(UserModel.email == email))
        return check.scalars().first()

    async def reset(self,email:str):
        check = await self.session.execute(select(UserModel).where(UserModel.email == email))
        user=check.scalars().first()
        return user

    async def update_password(self,user:UserModel):
        self.session.add(user)
        await self.session.commit()
        await self.session.refresh(user)
        return {"message":"Password is updated"}

    async def add_update_otp(self,otp:OtpModel):
        self.session.add(otp)
        await self.session.commit()
        await self.session.refresh(otp)
        return otp.otp

    async def check_email_for_otp(self,email:str):
        check = await self.session.execute(select(UserModel).where(UserModel.email == email))
        user=check.scalars().first()
        if not user:
            raise HTTPException(status_code=404, detail="No Such User Exists")
        else:
            # print("repo check")
            check1 = await self.session.execute(select(OtpModel).where(OtpModel.email == email))
            otp=check1.scalars().first()
            if otp:
                return otp
            else:
                return False


    async def get_from_otp(self,email:str):
        check1 = await self.session.execute(select(OtpModel).where(OtpModel.email == email))
        otp = check1.scalars().first()
        if otp:
            return otp
        else:
            raise HTTPException(status_code=401,detail="Generate Otp First")

    async def get_using_otp(self,otp:int):
        check1 = await self.session.execute(select(OtpModel).where(OtpModel.otp == otp))
        otp = check1.scalars().first()
        if otp:
            return otp
        else:
            raise HTTPException(status_code=401,detail="Invalid Otp")

    async def delete_otp(self,otp:OtpModel):
        await self.session.delete(otp)
        await self.session.commit()
        return {"message":"Password Reset Successful"}





