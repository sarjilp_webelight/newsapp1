from datetime import datetime,date
from typing import Optional, List, Any
from uuid import UUID

from fastapi_utils.api_model import APIModel



class CreateLikeResponse(APIModel):
    like_id: UUID
    user_id: UUID
    news_id: UUID

class CreateCommentResponse(APIModel):
    comment_id: UUID
    comment_content: str
    user_id: UUID
    news_id: UUID
    comment_date: date

class CreateCategoryResponse(APIModel):
    category_id: UUID
    category_name: str

class TagsNewsResponse(APIModel):
    news_id: UUID
    category_id: UUID

class CreateTagResponse(APIModel):
    tag_id: UUID
    tag_name: str


class CreateNewsResponse(APIModel):
    news_id: UUID
    news_title: str
    news_content: str
    is_active: bool
    publish_date: date
    city_id: int
    user_id: UUID
    likes:List[CreateLikeResponse]
    comments: List[CreateCommentResponse]
    category: List[CreateCategoryResponse]
    tags: List[CreateTagResponse]
    like_count: int
    comment_count: int


class CreateUserResponse(APIModel):
    user_id: UUID
    email: str
    fname: str
    lname: str
    role_id: UUID
    is_active: bool
    city_id: UUID
    news: List[CreateNewsResponse] = []
    likes: List[CreateLikeResponse]
    comments: List[CreateCommentResponse]


class SignUpResponse(APIModel):
    user_id: UUID
    email: str
    fname: str
    lname: str
    is_active: bool
    city_id: UUID




class CreateCityResponse(APIModel):
    city_id: UUID
    city_name: str
    users:List[CreateUserResponse]=[]
    news:List[CreateNewsResponse]=[]


class CreateRoleResponse(APIModel):
    role_id: UUID
    role_name: str
    users:List[CreateUserResponse]


class CategoryNewsResponse(APIModel):
    news_id: UUID
    category_id: UUID


class TagsNewsResponse(APIModel):
    news_id: UUID
    category_id: UUID
