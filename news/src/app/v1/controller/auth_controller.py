
from fastapi.security import HTTPBearer
from fastapi_utils.inferring_router import InferringRouter
from fastapi import Depends,Request
from fastapi import status
from fastapi_utils.cbv import cbv
from app.v1.schema import CreateUserRequestSchema,CreateUserResponse,LoginSchema, SignUpResponse
from app.v1.service import AuthCommandService

router=InferringRouter()
security = HTTPBearer()
@cbv(router=router)
class AuthController:
    auth_command_service:AuthCommandService = Depends(AuthCommandService)
    @router.post(
        "/signup",
        status_code=status.HTTP_200_OK,
        response_model=SignUpResponse,
        name="Sign Up"
    )
    async def signup(self,request:CreateUserRequestSchema):
        return await self.auth_command_service.signupuser(**request.dict())


    @router.post(
        "/signin",
        status_code=status.HTTP_200_OK,
        name="Sign In"
    )
    async def signin(self,request:LoginSchema):
        return await self.auth_command_service.login(**request.dict())


    @router.put(
        "/changePassword",
        status_code=status.HTTP_200_OK,
        name="Change Password",
        dependencies=[Depends(security)]
    )
    async def reset(self,old_password:str,new_password:str,request:Request):
        return await self.auth_command_service.reset(old_password,new_password,request)


    @router.get(
        "/generateOtp",
        status_code=status.HTTP_200_OK,
        name="Generate OTP"
    )
    async def get_otp(self,email:str):
        # print("cont check")
        return await self.auth_command_service.get_otp(email)


    @router.post(
        "/forgetPassword",
        status_code=status.HTTP_200_OK,
        name="Forget and Reset Password"
    )
    async def forget(self,otp:int,email:str,new_password:str):
        return await self.auth_command_service.forget(otp,email,new_password)

    @router.post(
        "/forgetPasswordWithoutMail",
        status_code=status.HTTP_200_OK,
        name="Forget and Reset Password w/o Email"
    )
    async def forgetwoemail(self, otp: int, new_password: str):
        return await self.auth_command_service.forget_noemail(otp, new_password)