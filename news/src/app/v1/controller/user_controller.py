from uuid import UUID
from typing import List
from fastapi import Depends, status, Request, HTTPException
from fastapi.security import HTTPBearer
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from core.utils.token import Token
from app.v1.schema import CreateUserRequestSchema, CreateUserResponse
from app.v1.service import UserCommandService

router = InferringRouter()
security = HTTPBearer()

@cbv(router=router)
class UserController:
    user_command_services: UserCommandService = Depends(UserCommandService, use_cache=True)

    @router.post(
        "/addUser",
        status_code=status.HTTP_200_OK,
        response_model=CreateUserResponse,
        name="Add User",
        dependencies=[Depends(security)]
    )
    async def add_user(self, request: CreateUserRequestSchema,head:Request):
        token=head.headers["authorization"]
        user_details=Token.get_details(token)
        print(user_details)
        if user_details["role_id"]!="c14daa81-be9d-4a7a-ab09-54f2e5e13bff":
            raise HTTPException(status_code=404,detail="Only Admin Can Access")
        res =await self.user_command_services.create_user(**request.dict())

        #res = CreateUserResponse.from_orm(res)
        return res

    @router.get(
        "/getUsers",
        status_code=status.HTTP_200_OK,
        response_model=List[CreateUserResponse],
        name="Get All Users"
    )
    async def get_all_users(self):
        return await self.user_command_services.read_users()

    @router.get(
        "/getUserById/{user_id}",
        status_code=status.HTTP_200_OK,
        response_model=CreateUserResponse,
        name="Get User By Id"
    )
    async def get_by_id(self, user_id: UUID):
        return await self.user_command_services.read_userid(user_id)

    @router.delete(
        "/deleteUser/{user_id}",
        status_code=status.HTTP_200_OK,
        name="Delete A User",
        dependencies=[Depends(security)]
    )
    async def delete_user(self, user_id: UUID,request:Request):
        token=request.headers["authorization"]
        user_details=Token.get_details(token)
        print(user_details)
        if user_details["role_id"]!="c14daa81-be9d-4a7a-ab09-54f2e5e13bff":
            raise HTTPException(status_code=404,detail="Only Admin Can Access")
        return await self.user_command_services.delete_user(user_id)

    @router.put(
        "/updateUser/{user_id}",
        status_code=status.HTTP_200_OK,
        response_model=CreateUserResponse,
        name="Update A User",
        dependencies=[Depends(security)]
    )
    async def update_user(self, user_id: UUID, request: CreateUserRequestSchema,head:Request):
        token=head.headers["authorization"]
        user_details=Token.get_details(token)
        print(user_details)
        if user_details["role_id"]!="c14daa81-be9d-4a7a-ab09-54f2e5e13bff":
            raise HTTPException(status_code=404,detail="Only Admin Can Access")
        return await self.user_command_services.update_user(user_id, request)

    @router.put(
        "/allotRole",
        status_code=status.HTTP_200_OK,
        name="Allot Role",
        dependencies=[Depends(security)]
    )
    async def allot_role(self,user_id:UUID,role_id:UUID,request:Request):
        token=request.headers["authorization"]
        user_details=Token.get_details(token)
        if user_details["role_id"]!="c14daa81-be9d-4a7a-ab09-54f2e5e13bff":
            raise HTTPException(status_code=404,detail="Only Admin Can Access")
        return await self.user_command_services.allot_role(user_id,role_id)

    @router.put(
        "/activateUser",
        status_code=status.HTTP_200_OK,
        name="Activate User",
        dependencies=[Depends(security)]
    )
    async def activate_user(self,user_id:UUID,request:Request):
        token=request.headers["authorization"]
        user_details=Token.get_details(token)
        if user_details["role_id"]!="d27f4643-849d-41c9-a887-290ba77c2f7d" and user_details["role_id"]!="c14daa81-be9d-4a7a-ab09-54f2e5e13bff":
            raise HTTPException(status_code=401,detail="Only Admin/Agency can access")
        return await self.user_command_services.activate_user(user_id)





