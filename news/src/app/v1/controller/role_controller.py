from fastapi_utils.inferring_router import InferringRouter
from fastapi import Depends
from fastapi import status
from fastapi_utils.cbv import cbv
from app.v1.service import  RoleCommandService
from app.v1.schema import CreateRoleRequestSchema,CreateRoleResponse
from uuid import UUID
router=InferringRouter()

@cbv(router=router)
class RoleController:

    role_command_service:RoleCommandService=Depends()

    @router.post(
        "/addRole",
        status_code=status.HTTP_200_OK,
        response_model=CreateRoleResponse,
        name="Add Roles"
    )
    async def add_roles(self,request:CreateRoleRequestSchema):
        return await self.role_command_service.create_role(**request.dict())


    @router.get(
        "/getRoles",
        status_code=status.HTTP_200_OK,
        name="Get All Roles"
    )
    async def get_all_news(self):
        return await self.role_command_service.get_all_roles()

    @router.get(
        "/getRoleId",
        status_code=status.HTTP_200_OK,
        name="Get Role By Id"
    )
    async def get_role_id(self,role_id:UUID):
        return await self.role_command_service.get_role_id(role_id)


    @router.delete(
        "/deleteRole",
        status_code=status.HTTP_200_OK,
        name="Delete a Role"
    )
    async def delete_role(self,role_id:UUID):
        return await self.role_command_service.delete_role(role_id)


    @router.put(
        "/updateRole",
        status_code=status.HTTP_200_OK,
        name="Update a Role"
    )
    async def update_role(self,role_id:UUID,request:CreateRoleRequestSchema):
        return await self.role_command_service.update_role(role_id,request)