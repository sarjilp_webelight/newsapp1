from fastapi import Depends, Request
from datetime import datetime, timedelta
from fastapi_utils.inferring_router import InferringRouter
from jose import JWTError,jwt
from fastapi.security import HTTPBearer



trial={"username":"Sarjil","password":"mypass","role_id":0}
SECRET_KEY="0c9d5fee5b70fc14ab3f02f07853bfea0316e73a70b26fa3a0808e5280aac766"
ACCESS_TOKEN_EXPIRE_MINUTES=30
ALGORITHM = "HS256"
access_time=timedelta(ACCESS_TOKEN_EXPIRE_MINUTES)
expire=datetime.utcnow()+access_time

router=InferringRouter()
security = HTTPBearer()






@router.post("/getToken",name="Generate Token")
async def getToken(username:str,password:str,role_id:int):
    trial["username"]=username
    trial["password"]=password
    trial["role_id"]=role_id
    trial.update({"exp":expire})
    encoded_jwt=jwt.encode(trial,SECRET_KEY,algorithm=ALGORITHM)
    return encoded_jwt

@router.get("/checkToken",name="Verify Token",dependencies=[Depends(security)])
async def getNews(request:Request):
    jwt_token=request.headers.get("authorization")
    jwt_token=jwt_token[7:]
    decoded_jwt=jwt.decode(jwt_token,SECRET_KEY)
    if decoded_jwt.get("role_id")==1:
        return {"message":"Access Granted"}
    return {"response":decoded_jwt}

