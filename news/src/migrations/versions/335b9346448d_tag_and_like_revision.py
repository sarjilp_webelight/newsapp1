"""Tag and Like Revision

Revision ID: 335b9346448d
Revises: 9947390bc357
Create Date: 2023-02-01 10:35:16.378310

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '335b9346448d'
down_revision = '9947390bc357'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('Likes',
    sa.Column('like_id', postgresql.UUID(as_uuid=True), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('news_id', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('like_id')
    )
    op.create_table('Tags',
    sa.Column('tag_id', postgresql.UUID(as_uuid=True), nullable=False),
    sa.Column('tag_name', sa.String(length=70), nullable=True),
    sa.PrimaryKeyConstraint('tag_id')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('Tags')
    op.drop_table('Likes')
    # ### end Alembic commands ###
