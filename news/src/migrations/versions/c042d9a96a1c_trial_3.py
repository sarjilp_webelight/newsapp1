"""trial 3

Revision ID: c042d9a96a1c
Revises: 14c4dac6c681
Create Date: 2023-02-07 11:08:03.920301

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c042d9a96a1c'
down_revision = '14c4dac6c681'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('Likes', 'user_id')
    op.drop_column('Likes', 'news_id')
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('Likes', sa.Column('news_id', sa.INTEGER(), autoincrement=False, nullable=True))
    op.add_column('Likes', sa.Column('user_id', sa.INTEGER(), autoincrement=False, nullable=True))
    # ### end Alembic commands ###
