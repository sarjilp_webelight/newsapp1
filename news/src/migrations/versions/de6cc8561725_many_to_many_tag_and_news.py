"""Many to Many Tag and News

Revision ID: de6cc8561725
Revises: 4ee58b01c6a7
Create Date: 2023-02-14 11:34:52.466772

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'de6cc8561725'
down_revision = '4ee58b01c6a7'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('tags_news',
    sa.Column('news_id', postgresql.UUID(as_uuid=True), nullable=True),
    sa.Column('tag_id', postgresql.UUID(as_uuid=True), nullable=True),
    sa.ForeignKeyConstraint(['news_id'], ['News.news_id'], ),
    sa.ForeignKeyConstraint(['tag_id'], ['Tags.tag_id'], )
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('tags_news')
    # ### end Alembic commands ###
